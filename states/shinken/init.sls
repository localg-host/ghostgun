shinken_user:
  user.present:
    - name: shinken

shinken_deps:
  pkg.installed:
    - pkgs:
      - python-pycurl
      - python-bottle
      - python-paramiko
      - python-cherrypy3
      - python-pip
      - monitoring-plugins

shinken_pip:
  pip.installed:
    - name: shinken
    - require:
      - pkg: shinken_deps
      - user: shinken

/var/run/shinken:
  file.directory:
    - user: shinken
    - group: shinken
    - recurse:
      - user
      - group

shinken --init:
  cmd.run:
    - user: shinken
    - unless: test -f /home/shinken/.shinken.ini
    - require:
      - pip: shinken_pip

shinken install auth-cfg-password:
  cmd.run:
    - user: shinken
    - unless: test -d /var/lib/shinken/modules/auth-cfg-password
    - require:
      - pip: shinken_pip

shinken install webui:
  cmd.run:
    - user: shinken
    - unless: test -d /var/lib/shinken/modules/webui
    - require:
      - pip: shinken_pip

shinken install ws-arbiter:
  cmd.run:
    - user: shinken
    - unless: test -d /var/lib/shinken/modules/ws-arbiter
    - require:
      - pip: shinken_pip

shinken install livestatus:
  cmd.run:
    - user: shinken
    - unless: test -d /var/lib/shinken/modules/livestatus
    - require:
      - pip: shinken_pip

shinken install http:
  cmd.run:
    - user: shinken
    - unless: test -d /etc/shinken/packs/http
    - require:
      - pip: shinken_pip

shinken install logstore-sqlite:
  cmd.run:
    - user: shinken
    - unless: test -d /var/lib/shinken/modules/logstore-sqlite
    - require:
      - pip: shinken_pip

/etc/shinken/modules/webui.cfg:
  file.managed:
    - source: salt://shinken/webui.cfg

/etc/shinken/brokers/broker-master.cfg:
  file.managed:
    - source: salt://shinken/broker-master.cfg

/etc/shinken/arbiters/arbiter-master.cfg:
  file.managed:
    - source: salt://shinken/arbiter-master.cfg

/etc/systemd/system/shinken-arbiter.service:
  file.managed:
    - source: salt://shinken/shinken-arbiter.service

/etc/systemd/system/shinken-broker.service:
  file.managed:
    - source: salt://shinken/shinken-broker.service

/etc/systemd/system/shinken-poller.service:
  file.managed:
    - source: salt://shinken/shinken-poller.service

/etc/systemd/system/shinken-reactionner.service:
  file.managed:
    - source: salt://shinken/shinken-reactionner.service

/etc/systemd/system/shinken-receiver.service:
  file.managed:
    - source: salt://shinken/shinken-receiver.service

/etc/systemd/system/shinken-scheduler.service:
  file.managed:
    - source: salt://shinken/shinken-scheduler.service

shinken_arbiter_service:
  service.running:
    - name: shinken-arbiter
    - enable: True
    - watch:
      - pip: shinken_pip
      - file: /var/run/shinken
      - file: /etc/systemd/system/shinken-arbiter.service
      - file: /etc/shinken/**/*

shinken_broker_service:
  service.running:
    - name: shinken-broker
    - enable: True
    - watch:
      - pip: shinken_pip
      - file: /var/run/shinken
      - file: /etc/systemd/system/shinken-broker.service
      - file: /etc/shinken/brokers/broker-master.cfg

shinken_poller_service:
  service.running:
    - name: shinken-poller
    - enable: True
    - watch:
      - pip: shinken_pip
      - file: /var/run/shinken
      - file: /etc/systemd/system/shinken-poller.service

shinken_reactionner_service:
  service.running:
    - name: shinken-reactionner
    - enable: True
    - watch:
      - pip: shinken_pip
      - file: /var/run/shinken
      - file: /etc/systemd/system/shinken-reactionner.service

shinken_receiver_service:
  service.running:
    - name: shinken-receiver
    - enable: True
    - watch:
      - pip: shinken_pip
      - file: /var/run/shinken
      - file: /etc/systemd/system/shinken-receiver.service

shinken_scheduler_service:
  service.running:
    - name: shinken-scheduler
    - enable: True
    - watch:
      - pip: shinken_pip
      - file: /var/run/shinken
      - file: /etc/systemd/system/shinken-scheduler.service

/etc/shinken/hosts/localhost.cfg:
  file.managed:
    - source: salt://shinken/host.jinja2
    - template: jinja
    - require:
      - pip: shinken_pip


include:
{%- for host, states in salt['state.show_top']('*').items() -%}
{%- for state in states %}
  - {{ state }}.monitoring
{%- endfor -%}
{%- endfor %}
