Quickstart
==========

Prerequisites
-------------

Install vagrant

Build the testing basebox :

.. code-block:: bash
  
  cd basebox
  vagrant up
  vagrant package
  vagrant box add package.box --name ghostgun/basebox --force
  rm package.box
  cd ..

The demo
--------

.. code-block:: bash
  
  vagrant up
  vagrant ssh -c "sudo salt-call state.s
  # one line should show a CRITICAL status with a red background
  # check http://192.168.50.122:7767/all
  vagrant ssh -c "sudo salt-call state.highstate"
  # wait a minute and check http://192.168.50.122:7767/all again
  # both lines should be green with status UP or OK
